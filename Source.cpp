#include <iostream>

using namespace std;

class Vector
{
private:
	double x{};
	double y{};
	double z{};
public:
	Vector(double a, double b, double c)
	{
		x = a;
		y = b;
		z = c;
	};
	void setVector(double a, double b, double c);
	int getX();
	int getY();
	int getZ();
	void outputVector();
};

void Vector::setVector(double a, double b, double c)
{
	x = a;
	y = b;
	z = c;
}

int Vector::getX()
{
	return x;
}

int Vector::getY()
{
	return y;
}

int Vector::getZ()
{
	return z;
}

void Vector::outputVector()
{
	cout << x << " " << y << " " << z << " ";
}

int main()
{
	Vector location(10.5553, 20.5553, 30.5553);
	location.outputVector();

	cout << "\n\n";

	location.setVector(location.getX() * 2, location.getY() * 2, location.getZ() * 2);
	location.outputVector();

	cout << "\n\n";
	return 0;
}